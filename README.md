# Grafana over Graphite
Grafana over Graphite provisioning through Vagrant + Docker

## How-To
You can set-up your local Grafana over Graphite environment simply cloning current repository and launching Vagrant:

```
git clone https://gitlab.com/darwinsw/grafana-over-graphite-provisioning.git
cd grafana-over-graphite-provisioning
vagrant up
```
Now you can access Grafana web app pointing to `http://localhost:3000` and using `secret` password for `admin` user.
You can (and you should!) change Grafana `admin` password changing the value of `GF_SECURITY_ADMIN_PASSWORD` environment variable in `docker-compose.yml`